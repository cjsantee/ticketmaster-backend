var express = require('express');
var router = express.Router();
const mongodb = require('mongodb');

const DB_NAME = 'ticket_master_backend';
const COLLECTION_NAME = 'Price_Zones';

const DB_URI = 'mongodb://localhost:27017';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, {useNewUrlParser: true, useUnifiedTopology: true});

client.connect(function(err, connection) {
  if (err) {
    return res.status(500).send({message: "Could not connect to the database."});
  }

  const db = connection.db(DB_NAME);

  // GET request
  router.get('/', function(req, res) {
    db.collection(COLLECTION_NAME)
      .find({})
        .toArray(function(find_error, result){
          if (find_error)
            return res.status(500).send({message: "Cound not retrieve collection data."});
          res.send(result);
    });
  });

  // POST request (INSERT Offer)
  router.post('/', function(req, res) {
    if (!req.body || req.body.length === 0)
      return res.status(400).send({message: "Object is required."});
    if (!req.body.type || !req.body.name || !req.body.currency)
      return res.status(400).send({message: "Required fields: type, name, currency"});

    var doc = req.body;

    db.collection(COLLECTION_NAME).insertOne(doc, function(insert_err, result) {
      if (insert_err)
        return res.status(500).send({message: "Could not insert object."});
      res.send('Object inserted.');
    });
  });

  // PUT request (UPDATE Offer)
  router.put('/:id', function(req, res) {
    db.collection(COLLECTION_NAME)
      .updateOne({"_id": objectId(req.params.id)},
        {$set: req.body},
          function(update_err, result) {
      if (update_err)
        return res.status(500).send({message: "Could not update object."});
      res.send('Object updated.');
    });
  });

  //DELETE request (DELETE Book)
  router.delete('/:id', function(req, res) {
    db.collection(COLLECTION_NAME)
      .deleteOne({"_id": objectId(req.params.id)},
        function(delete_err, result){
      if (delete_err)
        return res.status(500).send({message: "Could not delete object."});
      res.send('Object deleted.');
    });
  });

});

module.exports = router;
