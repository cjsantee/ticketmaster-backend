const express = require('express');
const mongodb = require('mongodb');
const bodyParser = require('body-parser');
var objectId = require('mongodb').ObjectID;

const areas = require('./apis/areas');
const attractions = require('./apis/attractions');
const events = require('./apis/events');
const genres = require('./apis/genres');
const images = require('./apis/images');
const offers = require('./apis/offers');
const passwords = require('./apis/passwords');
const price_zones = require('./apis/price-zones');
const prices = require('./apis/prices');
const products = require('./apis/products');
const segments = require('./apis/segments');
const sub_genres = require('./apis/sub-genres');
const venues = require('./apis/venues');

const app = express();
const PORT = 5050;

const DB_NAME = 'ticket_master_backend';

const DB_URI = 'mongodb://localhost:27017';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, {useNewUrlParser: true, useUnifiedTopology: true});

app.use(bodyParser.urlencoded({extended: false})); // allow user to send data within the url
app.use(bodyParser.json()); // allow user to send JSON dat

app.use('/areas', areas);
app.use('/attractions', attractions);
app.use('/events', events);
app.use('/genres', genres);
app.use('/images', images);
app.use('/offers', offers);
app.use('/passwords', passwords);
app.use('/price-zones', price_zones);
app.use('/prices', prices);
app.use('/products', products);
app.use('/segments', segments);
app.use('/sub-genres', sub_genres);
app.use('/venues', venues);

app.listen(PORT);
console.log("Listening on port " + PORT);
